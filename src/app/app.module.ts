import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { PostulanteFormComponent } from './rrhh/postulante-form/postulante-form.component';
import { PostulanteReporteComponent } from './rrhh/postulante-reporte/postulante-reporte.component';
import { UsuariosComponent } from './sec/usuarios/usuarios.component';
import { LoginComponent } from './sec/login/login.component';
import { CommonDataService } from './services/common-data.service';
import { DataRrhhService } from './services/data-rrhh.service';
import { SesionService } from './services/sesion.service';
import { NavegacionComponent } from './components/navegacion/navegacion.component';
import { PanelComponent } from './components/panel/panel.component';
import { IpdataService } from './services/ipdata.service';
import { UploadPhotoComponent } from './components/upload-photo/upload-photo.component';

@NgModule({
  declarations: [
    AppComponent,
    PostulanteFormComponent,
    UsuariosComponent,
    PostulanteReporteComponent,
    LoginComponent,
    NavegacionComponent,
    PanelComponent,
    UploadPhotoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
    MaterialModule
  ],
  providers: [
    CommonDataService,
    DataRrhhService,
    SesionService,
    IpdataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
