import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PanelComponent } from './components/panel/panel.component';
import { PostulanteFormComponent } from './rrhh/postulante-form/postulante-form.component';
import { PostulanteReporteComponent } from './rrhh/postulante-reporte/postulante-reporte.component';
import { LoginComponent } from './sec/login/login.component';
import { UsuariosComponent } from './sec/usuarios/usuarios.component';

const routes: Routes = [
  { path: '', component: PanelComponent },
  { path: 'login', component: LoginComponent },
  { path: 'reporte', component: PostulanteReporteComponent },
  { path: 'postulantes', component: PostulanteFormComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: '**', pathMatch: 'full', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
