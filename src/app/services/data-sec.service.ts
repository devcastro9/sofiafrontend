import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GcControles } from '../models/gccontrol.model';
import { GcFormulario } from '../models/gcformulario.model';
import { UsuarioModel } from '../models/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class DataSecService {

  constructor(private http: HttpClient) { }

  getFormularios(): Observable<GcFormulario[]> {
    return this.http.get<GcFormulario[]>(environment.apiUrl + 'api/GcFormularios');
  }

  getControles(idForm: number): Observable<GcControles[]> {
    return this.http.get<GcControles[]>(environment.apiUrl + 'api/GcControles/Form/' + idForm);
  }

  getUsuarios(): Observable<UsuarioModel[]> {
    return this.http.get<UsuarioModel[]>(environment.apiUrl + 'api/GcUsuarios');
  }
}
