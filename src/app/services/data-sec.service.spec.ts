import { TestBed } from '@angular/core/testing';

import { DataSecService } from './data-sec.service';

describe('DataSecService', () => {
  let service: DataSecService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataSecService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
