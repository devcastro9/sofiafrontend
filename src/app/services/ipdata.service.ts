import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Ipify } from '../models/ipify.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IpdataService {

  constructor(private http: HttpClient) { }

  public getIPAddress(): Observable<Ipify> {
    return this.http.get<Ipify>("http://api.ipify.org/?format=json");
  }

}
