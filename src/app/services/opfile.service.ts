import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { ResponseFile } from '../models/responsefile.model';

@Injectable({
  providedIn: 'root'
})
export class OpfileService {

  constructor(private http: HttpClient) { }

  uploadFile(file: File): Observable<HttpEvent<ResponseFile>> {
    const formData = new FormData();
    formData.append('file', file);
    return this.http.request(new HttpRequest(
      'POST',
      `${environment.apiUrl}Fotos`,
      formData,
      {
        reportProgress: true,
        responseType: "json"
      }));
  }
}
