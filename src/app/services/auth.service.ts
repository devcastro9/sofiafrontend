import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LoginBody } from '../models/login-body.model';
import { LoginResponse } from '../models/login-response.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private id_token: string = 'token_sistema';

  constructor(private http: HttpClient) { }

  login(model: LoginBody): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(environment.apiUrl + 'Session/Login', model, httpOptions);
  }

  setSession(response: LoginResponse): void {
    if (!response.isLogin) {
      return;
    }
    localStorage.setItem(this.id_token, response.jwt);
  }

  logout(): void {
    localStorage.removeItem(this.id_token);
  }

  isExpired() : boolean {
    const token = localStorage.getItem(this.id_token);
    if (token === null) {
      return true;
    }
    return false;
  }

}
