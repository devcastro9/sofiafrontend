import { TestBed } from '@angular/core/testing';

import { OpfileService } from './opfile.service';

describe('OpfileService', () => {
  let service: OpfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OpfileService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
