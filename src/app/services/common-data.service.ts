import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Departamento } from '../models/departamento.model';
import { environment } from 'src/environments/environment';
import { Pais } from '../models/pais.model';

@Injectable({
  providedIn: 'root'
})
export class CommonDataService {

  constructor(private http: HttpClient) { }

  getDepartamento(): Observable<Departamento[]> {
    return this.http.get<Departamento[]>(environment.apiUrl + 'Departamentos');
  }

  getPaises(filter: string): Observable<Pais[]> {
    return this.http.get<Pais[]>(environment.apiUrl + 'GcPaises/' + filter);
  }
}
