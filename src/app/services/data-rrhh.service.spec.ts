import { TestBed } from '@angular/core/testing';

import { DataRrhhService } from './data-rrhh.service';

describe('DataRrhhService', () => {
  let service: DataRrhhService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataRrhhService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
