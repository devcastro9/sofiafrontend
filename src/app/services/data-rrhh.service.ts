import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

import { Beneficiario } from '../models/beneficiario.model';
import { Educacion } from '../models/educacion.model';
import { EstadoCivil } from '../models/estadocivil.model';
import { Genero } from '../models/genero.model';
import { Puesto } from '../models/puesto.model';
import { TipoDocumento } from '../models/tipodocumento.model';
import { UnidadMedida } from '../models/unidadmedida.model';
import { Respuesta } from '../models/respuesta.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class DataRrhhService {

  constructor(private http: HttpClient) { }

  addPostulante(postulante: Beneficiario): Observable<Respuesta> {
    return this.http.post<Respuesta>(environment.apiUrl + 'Beneficiarios', postulante, httpOptions);
  }

  getEducacion(): Observable<Educacion[]> {
    return this.http.get<Educacion[]>(environment.apiUrl + 'RcNivelEducacionales');
  }

  getEstadoCivil(): Observable<EstadoCivil[]> {
    return this.http.get<EstadoCivil[]>(environment.apiUrl + 'RcEstadoCiviles');
  }

  getGenero(): Observable<Genero[]> {
    return this.http.get<Genero[]>(environment.apiUrl + 'Generos');
  }

  getPuesto(): Observable<Puesto[]> {
    return this.http.get<Puesto[]>(environment.apiUrl + 'Puestos');
  }

  getTipoDocumento(): Observable<TipoDocumento[]> {
    return this.http.get<TipoDocumento[]>(environment.apiUrl + 'GcTipoDocumentoIds');
  }

  getUnidadesMedida(): Observable<UnidadMedida[]> {
    return this.http.get<UnidadMedida[]>(environment.apiUrl + 'UnidadMedidaTiempos');
  }
}
