import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { OpfileService } from 'src/app/services/opfile.service';

@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.scss']
})
export class UploadPhotoComponent implements OnInit {

  selectImages?: FileList | null;
  selectNombre?: string;

  progreso: number = 0;
  mensaje: string = '';

  preview: string = '';

  ruta?: string;

  @Output() rutaResultado = new EventEmitter<string>();

  constructor(private fileServ: OpfileService) { }

  ngOnInit(): void { }

  selectFile(event: Event): void {
    // Reseteo de variables
    this.mensaje = '';
    this.progreso = 0;
    this.selectNombre = '';
    this.preview = '';
    this.rutaResultado.emit('');
    // Validacion de imagen
    this.selectImages = (event.target as HTMLInputElement).files;
    if (this.selectImages === null || this.selectImages === undefined || this.selectImages.length === 0) {
      this.mensaje = 'No se selecciono ninguna imagen';
      return;
    }
    const foto: File = this.selectImages[0];
    if (foto === undefined) {
      this.mensaje = 'Imagen no encontrada';
      return;
    }
    // Conversion a base64
    const reader = new FileReader();
    reader.onload = (): void => {
      this.preview = reader.result?.toString() ?? '';
    };
    reader.readAsDataURL(foto);
    // Nombre de archivo
    this.selectNombre = foto.name;
    // Metodo POST
    this.fileServ.uploadFile(foto).subscribe({
      next: (value) => {
        //console.log(typeof value);
        switch (value.type) {
          case HttpEventType.UploadProgress:
            this.progreso = Math.round((100 * value.loaded) / value.total!);
            //console.log('Progreso: ' + this.progreso);
            break;
          case HttpEventType.Response:
            this.ruta = value.body?.ruta;
            break;
          default:
            //console.log(value);
            break;
        }
      },
      error: (err: HttpErrorResponse) => {
        this.progreso = 0;
        this.mensaje = err.statusText;
      },
      complete: () => {
        this.rutaResultado.emit(this.ruta);
        this.mensaje = 'Carga completada';
      }
    });
  }
}
