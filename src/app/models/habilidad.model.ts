export interface Habilidad {
    descripcion: string;
    tiempo: number;
    unidadMedidaId: number;
}