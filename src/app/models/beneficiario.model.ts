import { Estudio } from "./estudio_realizado.model";
import { Experiencia } from "./experiencia_laboral.model";
import { Habilidad } from "./habilidad.model";
import { Idioma } from "./idioma.model";

export interface Beneficiario {
    puestoId: number;
    documentoIdentidad: string;
    documentoRespaldoId: string;
    departamentoId: number;
    primerApellido: string;
    segundoApellido: string;
    nombres: string;
    fechaNacimiento: Date;
    paisId: number;
    generoId: number;
    estadoCivilId: string;
    telefonoFijo: string;
    telefonoCelular: string;
    emailPersonal: string;
    domicilioLegal: string;
    licenciaConducir: boolean;
    licenciaMotocicleta: boolean;
    fotoRuta: string;
    pretensionSalarial: number;
    estudiosRealizados: Estudio[];
    experienciaLaborales: Experiencia[];
    habilidades: Habilidad[];
    idiomas: Idioma[];
}