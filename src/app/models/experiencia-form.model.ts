import { FormControl } from "@angular/forms";

export interface ExperienciaForm {
    nombreInstitucion: FormControl<string>;
    cargo: FormControl<string>;
    funcionGeneral: FormControl<string>;
    paisId: FormControl<number>;
    ciudad: FormControl<string>;
    fechaInicio: FormControl<Date>;
    fechaFinalizacion: FormControl<Date>;
}