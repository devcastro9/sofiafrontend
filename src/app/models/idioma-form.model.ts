import { FormControl } from "@angular/forms";

export interface IdiomaForm {
    idiomaDescripcion: FormControl<string>;
    nivelEscrito: FormControl<string>;
    nivelOral: FormControl<string>;
    nivelLectura: FormControl<string>;
}
