export interface TipoDocumento {
    tipodocCodigo: string;
    tipodocDescripcion: string;
}