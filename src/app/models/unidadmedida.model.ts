export interface UnidadMedida {
    idUnidadMedida: number;
    descripcion: string;
}