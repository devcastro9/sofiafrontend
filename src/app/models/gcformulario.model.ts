export interface GcFormulario {
  formId: number,
  archivoForm: string,
  nombreForm: string,
  caption: string,
  fechaSubido: Date
}
