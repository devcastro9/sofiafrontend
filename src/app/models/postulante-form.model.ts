import { FormArray, FormControl, FormGroup } from "@angular/forms";
import { EstudioForm } from "./estudio-form.model";
import { ExperienciaForm } from "./experiencia-form.model";
import { HabilidadForm } from "./habilidad-form.model";
import { IdiomaForm } from "./idioma-form.model";
import { Pais } from "./pais.model";

export interface PostulanteForm {
  puestoId: FormControl<number>;
  documentoIdentidad: FormControl<string>;
  documentoRespaldoId: FormControl<string>;
  departamentoId: FormControl<number>;
  primerApellido: FormControl<string>;
  segundoApellido: FormControl<string>;
  nombres: FormControl<string>;
  fechaNacimiento: FormControl<Date>;
  paisId: FormControl<number | Pais>;
  generoId: FormControl<number>;
  estadoCivilId: FormControl<string>;
  telefonoFijo: FormControl<string>;
  telefonoCelular: FormControl<string>;
  emailPersonal: FormControl<string>;
  domicilioLegal: FormControl<string>;
  licenciaConducir: FormControl<boolean>;
  licenciaMotocicleta: FormControl<boolean>;
  fotoRuta: FormControl<string>;
  pretensionSalarial: FormControl<number>;
  estudiosFormP: FormArray<FormGroup<EstudioForm>>;
  estudiosFormS: FormArray<FormGroup<EstudioForm>>;
  estudiosFormO: FormArray<FormGroup<EstudioForm>>;
  experienciaForm: FormArray<FormGroup<ExperienciaForm>>;
  habilidadForm: FormArray<FormGroup<HabilidadForm>>;
  idiomaForm: FormArray<FormGroup<IdiomaForm>>;
}
