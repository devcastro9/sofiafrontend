export interface Educacion {
    nivelEducCodigo: number;
    nivelEducDescripcion: string;
}