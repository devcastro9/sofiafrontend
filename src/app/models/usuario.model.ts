export interface UsuarioModel {
  usrCodigo: string;
  beneficiarioCodigo: string;
  usrNombres: string;
  usrPrimerApellido?: string;
  usrSegundoApellido?: string;
  usrClave: string;
  dgralCodigo?: string;
  daCodigo: string;
  unidadCodigo?: string;
  ocupCodigo?: number;
  usrObservaciones: string;
  idnivelacceso?: number;
  estadoCodigo: string;
  fechaRegistro: string;
  horaRegistro?: string;
  deptoCodigo: string;
  validarVersion: boolean;
  //gcUsuariosRoles: any[];
}
