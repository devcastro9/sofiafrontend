import { FormControl } from "@angular/forms";

export interface EstudioForm {
    centroEducativo: FormControl<string>;
    tituloObtenido: FormControl<string | null>;
    nivelEducativoId: FormControl<number>;
    tipo: FormControl<number>;
    ciudad: FormControl<string>;
    paisId: FormControl<number>;
    fechaInicio: FormControl<Date>;
    fechaFinalizacion: FormControl<Date>;
    estudiandoActualmente: FormControl<boolean>;
}