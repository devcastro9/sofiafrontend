export interface EstadoCivil {
    estadoCivilCodigo: string;
    estadoCivilDescripcion: string;
}