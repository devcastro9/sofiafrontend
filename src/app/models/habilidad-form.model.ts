import { FormControl } from "@angular/forms";

export interface HabilidadForm {
    descripcion: FormControl<string>;
    tiempo: FormControl<number>;
    unidadMedidaId: FormControl<number>;
}