export interface GcControles {
  ctrlId: number,
  formId: number,
  nombreControl: string,
  tipoControl: string,
  fechaModificado: Date,
}
