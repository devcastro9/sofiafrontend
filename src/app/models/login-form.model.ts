import { FormControl } from "@angular/forms";

export interface LoginForm {
  usuario: FormControl<string>;
  password: FormControl<string>;
}
