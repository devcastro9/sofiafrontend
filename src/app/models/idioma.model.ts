export interface Idioma {
    idiomaDescripcion: string;
    nivelEscrito: string;
    nivelOral: string;
    nivelLectura: string;
}