export interface Estudio {
    centroEducativo: string;
    tituloObtenido: string | null;
    nivelEducativoId: number;
    tipo: number;
    ciudad: string;
    paisId: number;
    fechaInicio: Date;
    fechaFinalizacion: Date;
    estudiandoActualmente: boolean;
}
