export interface Experiencia {
    nombreInstitucion: string;
    cargo: string;
    funcionGeneral: string;
    paisId: number;
    ciudad: string;
    fechaInicio: Date;
    fechaFinalizacion: Date;
}