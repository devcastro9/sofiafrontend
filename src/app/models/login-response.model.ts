export interface LoginResponse {
  isLogin: boolean,
  jwt: string,
  expires: Date,
  mensaje: string
}
