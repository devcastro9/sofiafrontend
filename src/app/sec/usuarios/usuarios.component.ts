import { Component, OnInit } from '@angular/core';
import { UsuarioModel } from 'src/app/models/usuario.model';
import { DataSecService } from 'src/app/services/data-sec.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  usuarios: UsuarioModel[] = [];

  constructor(private data: DataSecService) { }

  ngOnInit(): void {
    this.data.getUsuarios().subscribe(
      result => console.log(result)
    );
  }
}
