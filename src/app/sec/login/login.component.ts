import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from "rxjs/operators";
import { LoginBody } from 'src/app/models/login-body.model';
import { LoginForm } from 'src/app/models/login-form.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  // Variables
  title: string = 'Inicie sesión';
  message: string = 'Por favor, introduce tu usuario y contraseña';
  hidePassword: boolean = true;
  // Responsive
  destroyed = new Subject<void>();
  isPhone: boolean = false;
  // Formulario
  loginForm = new FormGroup<LoginForm>({
    usuario: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1)] }),
    password: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1)] })
  });

  constructor(private responsive: BreakpointObserver, private auths: AuthService) { }

  ngOnInit(): void {
    this.responsive
      .observe([Breakpoints.XSmall])
      .pipe(takeUntil(this.destroyed))
      .subscribe((result): void => {
        this.isPhone = result.matches;
      });
  }

  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

  inicioSesion(): void {
    let fecha: Date;
    if (this.loginForm.valid) {
      let loginBody: LoginBody = {
        usuario: this.loginForm.controls.usuario.value,
        password: this.loginForm.controls.password.value
      };
      this.auths.login(loginBody).subscribe(
        result => {
          console.log(result);
          fecha = result.expires;
          console.log(fecha);
        }
      );
    }
  }
}
