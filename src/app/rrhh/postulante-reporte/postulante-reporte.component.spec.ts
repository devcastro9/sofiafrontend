import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PostulanteReporteComponent } from './postulante-reporte.component';

describe('PostulanteReporteComponent', () => {
  let component: PostulanteReporteComponent;
  let fixture: ComponentFixture<PostulanteReporteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PostulanteReporteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PostulanteReporteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
