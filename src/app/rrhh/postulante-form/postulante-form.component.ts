import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, EMPTY } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, shareReplay, startWith, switchMap } from 'rxjs/operators';

import { Beneficiario } from 'src/app/models/beneficiario.model';
import { Departamento } from 'src/app/models/departamento.model';
import { Educacion } from 'src/app/models/educacion.model';
import { EstadoCivil } from 'src/app/models/estadocivil.model';
import { Estudio } from 'src/app/models/estudio_realizado.model';
import { EstudioForm } from 'src/app/models/estudio-form.model';
import { Experiencia } from 'src/app/models/experiencia_laboral.model';
import { ExperienciaForm } from 'src/app/models/experiencia-form.model';
import { Genero } from 'src/app/models/genero.model';
import { Habilidad } from 'src/app/models/habilidad.model';
import { HabilidadForm } from 'src/app/models/habilidad-form.model';
import { Idioma } from 'src/app/models/idioma.model';
import { IdiomaForm } from 'src/app/models/idioma-form.model';
import { Pais } from 'src/app/models/pais.model';
import { PostulanteForm } from 'src/app/models/postulante-form.model';
import { Puesto } from 'src/app/models/puesto.model';
import { TipoDocumento } from 'src/app/models/tipodocumento.model';
import { UnidadMedida } from 'src/app/models/unidadmedida.model';

import { CommonDataService } from 'src/app/services/common-data.service';
import { DataRrhhService } from 'src/app/services/data-rrhh.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-postulante-form',
  templateUrl: './postulante-form.component.html',
  styleUrls: ['./postulante-form.component.scss']
})
export class PostulanteFormComponent implements OnInit {
  // Variables
  titulo: string = 'Formulario de Postulación OTIS Bolivia';
  btnDisabled: boolean = false;
  // Datos para Select
  Departamentos: Departamento[] = [];
  NEducaciones: Educacion[] = [];
  EstadoCivil: EstadoCivil[] = [];
  Generos: Genero[] = [];
  Puestos: Puesto[] = [];
  TipoDocumentos: TipoDocumento[] = [];
  UnidadMedidas: UnidadMedida[] = [];
  // Datos para Autocomplete
  paisesOpciones!: Observable<Pais[]>;
  //Paises: Pais[] = [];
  // Responsive
  isXSmall$: Observable<boolean> = this.responsive.observe(Breakpoints.XSmall)
    .pipe(
      map(result => {
        return result.matches;
      }),
      shareReplay(),
    );
  // Formulario
  postulanteForm = new FormGroup<PostulanteForm>({
    puestoId: new FormControl<number>(0, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
    documentoIdentidad: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(20)] }),
    documentoRespaldoId: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(3)] }),
    departamentoId: new FormControl<number>(0, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
    primerApellido: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(35)] }),
    segundoApellido: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(35)] }),
    nombres: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50)] }),
    fechaNacimiento: new FormControl<Date>(new Date(), { nonNullable: true, validators: [Validators.required] }),
    paisId: new FormControl<number | Pais>(0, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
    generoId: new FormControl<number>(0, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
    estadoCivilId: new FormControl<string>('', { nonNullable: true, validators: [Validators.required] }),
    telefonoFijo: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(30)] }),
    telefonoCelular: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(30)] }),
    emailPersonal: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')] }),
    domicilioLegal: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(250)] }),
    licenciaConducir: new FormControl<boolean>(false, { nonNullable: true }),
    licenciaMotocicleta: new FormControl<boolean>(false, { nonNullable: true }),
    fotoRuta: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(3)] }),
    pretensionSalarial: new FormControl<number>(0, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
    estudiosFormP: new FormArray<FormGroup<EstudioForm>>([], { validators: Validators.required }),
    estudiosFormS: new FormArray<FormGroup<EstudioForm>>([], { validators: Validators.required }),
    estudiosFormO: new FormArray<FormGroup<EstudioForm>>([], { validators: Validators.required }),
    experienciaForm: new FormArray<FormGroup<ExperienciaForm>>([], { validators: Validators.required }),
    habilidadForm: new FormArray<FormGroup<HabilidadForm>>([], { validators: Validators.required }),
    idiomaForm: new FormArray<FormGroup<IdiomaForm>>([], { validators: Validators.required })
  });

  constructor(private dataCommon: CommonDataService, private dataRRHH: DataRrhhService, private responsive: BreakpointObserver) { }

  ngOnInit(): void {
    // Datos
    this.dataCommon.getDepartamento().subscribe(
      (result): Departamento[] => this.Departamentos = result
    );
    this.dataRRHH.getEducacion().subscribe(
      (result): Educacion[] => this.NEducaciones = result
    );
    this.dataRRHH.getEstadoCivil().subscribe(
      (result): EstadoCivil[] => this.EstadoCivil = result
    );
    this.dataRRHH.getGenero().subscribe(
      (result): Genero[] => this.Generos = result
    );
    this.dataRRHH.getPuesto().subscribe(
      (result): Puesto[] => this.Puestos = result
    );
    this.dataRRHH.getTipoDocumento().subscribe(
      (result): TipoDocumento[] => this.TipoDocumentos = result
    );
    this.dataRRHH.getUnidadesMedida().subscribe(
      (result): UnidadMedida[] => this.UnidadMedidas = result
    );
    // Observable
    this.paisesOpciones = this.postulanteForm.controls.paisId.valueChanges.pipe(
      startWith(''),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(value => {
        if (typeof value === 'string') {
          //console.log('Observable 1..' + value);
          return this._filter(value.toString() || 'bol');
        }
        //console.log('Observable 2..' + value);
        return EMPTY;
      })
    );
  }

  private _filter(value: string): Observable<Pais[]> {
    const filterValue = value.toLowerCase();
    //console.log('Filtrado...' + value);
    return this.dataCommon.getPaises(filterValue);
  }

  displayFn(pais: Pais): string {
    return pais && pais.paisDescripcion ? pais.paisDescripcion : '';
  }

  addEducacion(tipoN: number): void {
    switch (tipoN) {
      case 1:
      case 2:
        const ctrl12 = new FormGroup<EstudioForm>({
          centroEducativo: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(80)] }),
          tituloObtenido: new FormControl<string>(''),
          nivelEducativoId: new FormControl<number>(tipoN + 1, { nonNullable: true }),
          tipo: new FormControl<number>(tipoN, { nonNullable: true }),
          ciudad: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50)] }),
          paisId: new FormControl<number>(31, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
          fechaInicio: new FormControl<Date>(new Date(), { nonNullable: true, validators: [Validators.required] }),
          fechaFinalizacion: new FormControl<Date>(new Date(), { nonNullable: true, validators: [Validators.required] }),
          estudiandoActualmente: new FormControl<boolean>(false, { nonNullable: true })
        });
        if (tipoN === 1) {
          this.postulanteForm.controls.estudiosFormP.push(ctrl12);
        } else {
          this.postulanteForm.controls.estudiosFormS.push(ctrl12);
        }
        break;
      case 3:
        const ctrl3 = new FormGroup<EstudioForm>({
          centroEducativo: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(80)] }),
          tituloObtenido: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(80)] }),
          nivelEducativoId: new FormControl<number>(0, { nonNullable: true, validators: [Validators.required, Validators.min(4)] }),
          tipo: new FormControl<number>(tipoN, { nonNullable: true }),
          ciudad: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50)] }),
          paisId: new FormControl<number>(31, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
          fechaInicio: new FormControl<Date>(new Date(), { nonNullable: true, validators: [Validators.required] }),
          fechaFinalizacion: new FormControl<Date>(new Date(), { nonNullable: true, validators: [Validators.required] }),
          estudiandoActualmente: new FormControl<boolean>(false, { nonNullable: true })
        });
        this.postulanteForm.controls.estudiosFormO.push(ctrl3);
        break;
      default:
        break;
    }
  }

  addExperiencia(): void {
    const experiencia = new FormGroup<ExperienciaForm>({
      nombreInstitucion: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50)] }),
      cargo: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50)] }),
      funcionGeneral: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(500)] }),
      paisId: new FormControl<number>(31, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
      ciudad: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(50)] }),
      fechaInicio: new FormControl<Date>(new Date(), { nonNullable: true, validators: [Validators.required] }),
      fechaFinalizacion: new FormControl<Date>(new Date(), { nonNullable: true, validators: [Validators.required] })
    });
    this.postulanteForm.controls.experienciaForm.push(experiencia);
  }

  addHabilidad(): void {
    const habilidad = new FormGroup<HabilidadForm>({
      descripcion: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(150)] }),
      tiempo: new FormControl<number>(0, { nonNullable: true, validators: [Validators.required, Validators.min(1)] }),
      unidadMedidaId: new FormControl<number>(0, { nonNullable: true, validators: [Validators.required, Validators.min(1)] })
    });
    this.postulanteForm.controls.habilidadForm.push(habilidad);
  }

  addIdioma(): void {
    const idioma = new FormGroup<IdiomaForm>({
      idiomaDescripcion: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(100)] }),
      nivelEscrito: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(150)] }),
      nivelOral: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(150)] }),
      nivelLectura: new FormControl<string>('', { nonNullable: true, validators: [Validators.required, Validators.minLength(1), Validators.maxLength(150)] })
    });
    this.postulanteForm.controls.idiomaForm.push(idioma);
  }

  delEducacion(index: number, tipo: number): void {
    switch (tipo) {
      case 1:
        this.postulanteForm.controls.estudiosFormP.removeAt(index);
        break;
      case 2:
        this.postulanteForm.controls.estudiosFormS.removeAt(index);
        break;
      case 3:
        this.postulanteForm.controls.estudiosFormO.removeAt(index);
        break;
      default:
        break;
    }
  }

  delExperiencia(index: number): void {
    this.postulanteForm.controls.experienciaForm.removeAt(index);
  }

  delHabilidad(index: number): void {
    this.postulanteForm.controls.habilidadForm.removeAt(index);
  }

  delIdioma(index: number): void {
    this.postulanteForm.controls.idiomaForm.removeAt(index);
  }

  getRutaFoto(e: string): void {
    if (e.length > 1) {
      this.postulanteForm.controls.fotoRuta.setValue(e);
    } else {
      this.postulanteForm.controls.fotoRuta.reset();
    }
  }

  Guardar(): void {
    // Creacion del JSON
    if (this.postulanteForm.valid) {
      this.btnDisabled = true;
      let postulante: Beneficiario = {
        puestoId: this.postulanteForm.controls.puestoId.value,
        documentoIdentidad: this.postulanteForm.controls.documentoIdentidad.value,
        documentoRespaldoId: this.postulanteForm.controls.documentoRespaldoId.value,
        departamentoId: this.postulanteForm.controls.departamentoId.value,
        primerApellido: this.postulanteForm.controls.primerApellido.value,
        segundoApellido: this.postulanteForm.controls.segundoApellido.value,
        nombres: this.postulanteForm.controls.nombres.value,
        fechaNacimiento: this.postulanteForm.controls.fechaNacimiento.value,
        paisId: (typeof this.postulanteForm.value.paisId === 'number') ? this.postulanteForm.value.paisId : this.postulanteForm.value.paisId?.id ?? 31,
        generoId: this.postulanteForm.controls.generoId.value,
        estadoCivilId: this.postulanteForm.controls.estadoCivilId.value,
        telefonoFijo: this.postulanteForm.controls.telefonoFijo.value,
        telefonoCelular: this.postulanteForm.controls.telefonoCelular.value,
        emailPersonal: this.postulanteForm.controls.emailPersonal.value,
        domicilioLegal: this.postulanteForm.controls.domicilioLegal.value,
        licenciaConducir: this.postulanteForm.controls.licenciaConducir.value,
        licenciaMotocicleta: this.postulanteForm.controls.licenciaMotocicleta.value,
        fotoRuta: this.postulanteForm.controls.fotoRuta.value,
        pretensionSalarial: this.postulanteForm.controls.pretensionSalarial.value,
        estudiosRealizados: [],
        experienciaLaborales: [],
        habilidades: [],
        idiomas: []
      };
      this.postulanteForm.value.estudiosFormP?.forEach(e => {
        const primary: Estudio = {
          centroEducativo: e.centroEducativo ?? '-',
          tituloObtenido: e.tituloObtenido ?? '-',
          nivelEducativoId: e.nivelEducativoId ?? 0,
          tipo: e.tipo ?? 1,
          ciudad: e.ciudad ?? '-',
          paisId: e.paisId ?? 31,
          fechaInicio: e.fechaInicio ?? new Date(),
          fechaFinalizacion: e.fechaFinalizacion ?? new Date(),
          estudiandoActualmente: e.estudiandoActualmente ?? false
        };
        postulante.estudiosRealizados.push(primary);
      });
      this.postulanteForm.value.estudiosFormS?.forEach(e => {
        const secondary: Estudio = {
          centroEducativo: e.centroEducativo ?? '-',
          tituloObtenido: e.tituloObtenido ?? '-',
          nivelEducativoId: e.nivelEducativoId ?? 0,
          tipo: e.tipo ?? 2,
          ciudad: e.ciudad ?? '-',
          paisId: e.paisId ?? 31,
          fechaInicio: e.fechaInicio ?? new Date(),
          fechaFinalizacion: e.fechaFinalizacion ?? new Date(),
          estudiandoActualmente: e.estudiandoActualmente ?? false
        };
        postulante.estudiosRealizados.push(secondary);
      });
      this.postulanteForm.value.estudiosFormO?.forEach(e => {
        const higher: Estudio = {
          centroEducativo: e.centroEducativo ?? '-',
          tituloObtenido: e.tituloObtenido ?? '-',
          nivelEducativoId: e.nivelEducativoId ?? 0,
          tipo: e.tipo ?? 3,
          ciudad: e.ciudad ?? '-',
          paisId: e.paisId ?? 31,
          fechaInicio: e.fechaInicio ?? new Date(),
          fechaFinalizacion: e.fechaFinalizacion ?? new Date(),
          estudiandoActualmente: e.estudiandoActualmente ?? false
        };
        postulante.estudiosRealizados.push(higher);
      });
      this.postulanteForm.value.experienciaForm?.forEach(e => {
        const experience: Experiencia = {
          nombreInstitucion: e.nombreInstitucion ?? '-',
          cargo: e.cargo ?? '-',
          funcionGeneral: e.funcionGeneral ?? '-',
          paisId: e.paisId ?? 31,
          ciudad: e.ciudad ?? '-',
          fechaInicio: e.fechaInicio ?? new Date(),
          fechaFinalizacion: e.fechaFinalizacion ?? new Date()
        };
        postulante.experienciaLaborales.push(experience);
      });
      this.postulanteForm.value.habilidadForm?.forEach(e => {
        const ability: Habilidad = {
          descripcion: e.descripcion ?? '-',
          tiempo: e.tiempo ?? 1,
          unidadMedidaId: e.unidadMedidaId ?? 1
        };
        postulante.habilidades.push(ability);
      });
      this.postulanteForm.value.idiomaForm?.forEach(e => {
        const language: Idioma = {
          idiomaDescripcion: e.idiomaDescripcion ?? '-',
          nivelEscrito: e.nivelEscrito ?? '-',
          nivelOral: e.nivelOral ?? '-',
          nivelLectura: e.nivelLectura ?? '-'
        };
        postulante.idiomas.push(language);
      });
      // JSON
      //console.log(JSON.stringify(postulante));
      // Envio de peticion
      this.dataRRHH.addPostulante(postulante).subscribe({
        next: (value) => {
          //console.log(value);
          if (value.Error) {
            Swal.fire('Ha ocurrido un error al guardar', value.Mensaje, 'error');
          }
        },
        error: (err: HttpErrorResponse) => {
          Swal.fire('Ha ocurrido un error en la red', err.statusText, 'error');
        },
        complete: () => {
          Swal.fire('Postulacion guardada', 'Se lo redirigira a la pagina de inicio en 3 segundos', 'success');
          setTimeout(() => {
            window.location.href = "https://otisbolivia.com/index.php/empresa/trabaja-con-nosotros";
          }, 3000);
        }
      });
      this.btnDisabled = false;
    } else {
      const toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
      toast.fire({
        icon: 'error',
        title: 'Complete todos los campos'
      });
    }
  }
}
